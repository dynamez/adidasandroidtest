package cl.rayout.adidastest.response

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


@Entity
class GoalResponse {


    @SerializedName(value = "items")
    @Expose
    var goals: List<Goal> = ArrayList()


    class Goal constructor(
        val id: Int,
        val title: String,
        val description: String,
        val type: String,
        val goal: Int


    )
}