package cl.rayout.adidastest.di.modules

import android.content.Context
import com.jakewharton.picasso.OkHttp3Downloader
import cl.rayout.adidastest.di.qualifiers.ApplicationContextQualifier
import cl.rayout.adidastest.di.scopes.CustomApplicationScope
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient

@Module(includes = [NetworkModule::class])
class PicassoModule {

    @Provides
    @CustomApplicationScope
    fun getOkHttp3Downloader(okHttpClient: OkHttpClient) = OkHttp3Downloader(okHttpClient)

    @Provides
    @CustomApplicationScope
    fun getPicasso(@ApplicationContextQualifier context: Context, okHttpDownloader: OkHttp3Downloader): Picasso {
        return Picasso.Builder(context)
            .downloader(okHttpDownloader)
            .build()
    }
}
