package cl.rayout.adidastest.di.qualifiers

import javax.inject.Qualifier

@Qualifier
annotation class ApplicationContextQualifier