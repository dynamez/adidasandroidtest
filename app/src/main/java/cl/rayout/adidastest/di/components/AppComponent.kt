package cl.rayout.adidastest.di.components

import cl.rayout.adidastest.backend.ApiService
import cl.rayout.adidastest.di.modules.PicassoModule
import cl.rayout.adidastest.di.modules.ServiceUtilModule
import cl.rayout.adidastest.di.scopes.CustomApplicationScope
import com.squareup.picasso.Picasso
import dagger.Component

@CustomApplicationScope
@Component(modules = [PicassoModule::class, ServiceUtilModule::class])
interface AppComponent {

    fun getPicasso(): Picasso

    fun getApiService(): ApiService
}
