package cl.rayout.adidastest.di.modules

import android.content.Context
import cl.rayout.adidastest.di.qualifiers.ApplicationContextQualifier
import cl.rayout.adidastest.di.scopes.CustomApplicationScope
import dagger.Module
import dagger.Provides

@Module
class ApplicationContextModule(private var context: Context) {

    @Provides
    @CustomApplicationScope
    @ApplicationContextQualifier
    fun getContext() = context
}
