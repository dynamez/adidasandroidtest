package cl.rayout.adidastest.viewHolder

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.TextView
import cl.rayout.adidastest.R
import kotlinx.android.synthetic.main.single_goal_item.view.*

class GoalViewHolder (itemView1: View) : RecyclerView.ViewHolder(itemView1), View.OnClickListener {

    //val moviePosterImageView: ImageView = itemView1.findViewById(R.id.moviePosterImageView)
    val goalTypeTextView: TextView = itemView1.findViewById(R.id.tipo)
    val goalTitleTextView: TextView = itemView1.findViewById(R.id.titulo)
    val goalDescriptionTextView: TextView = itemView1.findViewById(R.id.descripcion)

    val cardView: CardView = itemView1.card_view

    override fun onClick(v: View) {
        when(v){
            cardView->doSomething()
            else->Log.d("click", "Hello")
        }

    }

    fun doSomething(){
        Log.d("do", "Something")
    }
}