package cl.rayout.adidastest.adapter

import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cl.rayout.adidastest.R
import cl.rayout.adidastest.activites.mainActivity.ui.DetailActivity
import cl.rayout.adidastest.response.GoalResponse
import cl.rayout.adidastest.viewHolder.GoalViewHolder
import com.squareup.picasso.Picasso

class GoalAdapter constructor(
    private val context: Context,
    private val goals: List<GoalResponse.Goal>,
    private val picasso: Picasso = Picasso.with(context)

) :
    RecyclerView.Adapter<GoalViewHolder>() {



    private val layoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): GoalViewHolder =
        GoalViewHolder(layoutInflater.inflate(R.layout.single_goal_item, p0, false))

    override fun getItemCount() = goals.size

    override fun onBindViewHolder(holder: GoalViewHolder, position: Int) {
        val goal = goals[position]
        //val url = IMAGE_BASE_URL.plus(movie.poster_path)
        //picasso.load(url).into(holder.moviePosterImageView)
        Log.i("GOAL",goal.toString())
        holder.goalTitleTextView.text = goal.title
        holder.goalDescriptionTextView.text = goal.description
        holder.goalTypeTextView.text = goal.type
        holder.cardView.setOnClickListener {

            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra("id", goal.id)
            intent.putExtra("title", goal.title)
            intent.putExtra("description", goal.description)
            intent.putExtra("type", goal.type)
            intent.putExtra("goal", goal.goal)
            context.startActivity(intent)

        }
    }


}