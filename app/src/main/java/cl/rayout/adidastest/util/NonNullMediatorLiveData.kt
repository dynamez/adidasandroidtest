package cl.rayout.adidastest.util

import android.arch.lifecycle.MediatorLiveData

class NonNullMediatorLiveData<T> : MediatorLiveData<T>()
