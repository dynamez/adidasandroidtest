package cl.rayout.adidastest.activites.mainActivity.dataSource

import android.util.Log
import cl.rayout.adidastest.backend.ApiService
import cl.rayout.adidastest.response.GoalResponse
import cl.rayout.adidastest.response.Result
import cl.rayout.adidastest.util.safeApiCall
import java.io.IOException

@Suppress("EXPERIMENTAL_FEATURE_WARNING")
class GoalDataSource constructor(private val apiService: ApiService) {





    suspend fun getGoals() = safeApiCall(
        call = { allGoals() },
        errorMessage = "Error happened"
    )

    private suspend fun allGoals(): Result<GoalResponse>{
        val response = apiService.getGoals().await()
        if (response.isSuccessful){
            Log.i("TAG GOALS", response.body().toString())

            return Result.Success(response.body()!!)
        }

        return Result.Error(IOException("Error ocurred during fetchhing goals"))
    }
}
