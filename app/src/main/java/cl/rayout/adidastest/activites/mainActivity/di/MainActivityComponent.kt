package cl.rayout.adidastest.activites.mainActivity.di

import cl.rayout.adidastest.activites.mainActivity.ui.MainActivity
import cl.rayout.adidastest.activites.mainActivity.viewModel.MainActivityViewModel
import cl.rayout.adidastest.di.components.AppComponent
import dagger.Component

@MainActivityScope
@Component(modules = [MainActivityModule::class], dependencies = [AppComponent::class])
interface MainActivityComponent : AppComponent {

    fun inject(mainActivity: MainActivity)

    fun viewModel(): MainActivityViewModel
}