package cl.rayout.adidastest.activites.mainActivity.viewModel

import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import cl.rayout.adidastest.activites.mainActivity.dataSource.GoalDataSource

@Suppress("UNCHECKED_CAST")
class MainActivityViewModelFactory constructor(private val goalDataSource: GoalDataSource) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>) = MainActivityViewModel(goalDataSource) as T
}