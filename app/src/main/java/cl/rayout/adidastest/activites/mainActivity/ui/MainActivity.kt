package cl.rayout.adidastest.activites.mainActivity.ui

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.view.View
import android.widget.Toast
import cl.rayout.adidastest.R
import cl.rayout.adidastest.activites.BaseActivity
import cl.rayout.adidastest.activites.mainActivity.di.DaggerMainActivityComponent
import cl.rayout.adidastest.activites.mainActivity.di.MainActivityModule
import cl.rayout.adidastest.activites.mainActivity.viewModel.MainActivityViewModel
import cl.rayout.adidastest.adapter.GoalAdapter
import cl.rayout.adidastest.response.GoalResponse
import cl.rayout.adidastest.util.nonNull
import cl.rayout.adidastest.util.observe
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.fitness.Fitness
import com.google.android.gms.fitness.FitnessOptions
import com.google.android.gms.fitness.data.DataType
import com.google.android.gms.fitness.data.Field
import com.google.android.gms.tasks.OnFailureListener
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity() {

    private lateinit var goalAdapter: GoalAdapter

    private val goals = mutableListOf<GoalResponse.Goal>()
    @Inject
    lateinit var picasso: Picasso
    @Inject
    lateinit var viewModel: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        DaggerMainActivityComponent.builder()
            .mainActivityModule(MainActivityModule(this))
            .appComponent(getAppComponent())
            .build()
            .inject(this)
        goalsRecyclerView.layoutManager = GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false)
        viewModel = ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
        goalsRecyclerView.setHasFixedSize(true)
        goalAdapter = GoalAdapter(this, goals, picasso)
        goalsRecyclerView.adapter = goalAdapter
        startListeningGoals()
        listenToErrors()


        val fitnessOptions = FitnessOptions.builder()
            .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
            .addDataType(DataType.AGGREGATE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
            .build()

        if (!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(this), fitnessOptions)) {
            GoogleSignIn.requestPermissions(
                this, // your activity
                GOOGLE_FIT_PERMISSIONS_REQUEST_CODE,
                GoogleSignIn.getLastSignedInAccount(this),
                fitnessOptions);
        } else {
            readData()
        }
    }
    private fun readData() {
        Fitness.getHistoryClient(this, GoogleSignIn.getLastSignedInAccount(this)!!)
            .readDailyTotal(DataType.TYPE_STEP_COUNT_DELTA)
            .addOnSuccessListener { dataSet ->
                val total = (if (dataSet.isEmpty)
                    0
                else
                    dataSet.dataPoints[0].getValue(Field.FIELD_STEPS).asInt()).toLong()
                Log.i(LOG_TAG, "Total steps: $total")
            }
            .addOnFailureListener(
                object: OnFailureListener {
                    override fun onFailure(e:Exception) {
                        Log.w(LOG_TAG, "There was a problem getting the step count.", e)
                    }
                })
    }
    private fun startListeningGoals() {
        viewModel
            .goals

            .nonNull()
            .observe(this) {
                progressBar.visibility = View.GONE
                this.goals.addAll(it)
                goalAdapter.notifyDataSetChanged()
            }
    }


    private fun listenToErrors() {
        viewModel.errors
            .nonNull()
            .observe(this) {
                progressBar.visibility = View.GONE
                Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
            }
    }

    companion object {
        const val GOOGLE_FIT_PERMISSIONS_REQUEST_CODE = 1
        const val LOG_TAG = "fitness activity"
        const val ADD_GOAL_REQUEST = 1
        const val EDIT_GOAL_REQUEST = 2
    }
}
