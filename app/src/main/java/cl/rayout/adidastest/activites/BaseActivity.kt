package cl.rayout.adidastest.activites

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import cl.rayout.adidastest.backend.MyCustomApplicationClass

@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity() {

    protected fun getAppComponent() = getApp().getAppComponent()

    private fun getApp() = applicationContext as MyCustomApplicationClass
}