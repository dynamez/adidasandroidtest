package cl.rayout.adidastest.activites.mainActivity.di

import android.arch.lifecycle.ViewModelProviders
import cl.rayout.adidastest.activites.mainActivity.dataSource.GoalDataSource
import cl.rayout.adidastest.activites.mainActivity.ui.MainActivity
import cl.rayout.adidastest.activites.mainActivity.viewModel.MainActivityViewModel
import cl.rayout.adidastest.activites.mainActivity.viewModel.MainActivityViewModelFactory
import cl.rayout.adidastest.backend.ApiService
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule constructor(private val mainActivity: MainActivity) {

    @Provides
    @MainActivityScope
    fun movieDataSource(apiService: ApiService) = GoalDataSource(apiService)

    @Provides
    @MainActivityScope
    fun mainActivityViewModelFactory(goalDataSource: GoalDataSource) = MainActivityViewModelFactory(goalDataSource)

    @Provides
    @MainActivityScope
    fun mainActivityViewModel(viewModelFactory: MainActivityViewModelFactory): MainActivityViewModel =
        ViewModelProviders.of(mainActivity, viewModelFactory).get(MainActivityViewModel::class.java)
}