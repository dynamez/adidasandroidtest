package cl.rayout.adidastest.activites.mainActivity.viewModel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import cl.rayout.adidastest.activites.mainActivity.dataSource.GoalDataSource

import cl.rayout.adidastest.response.GoalResponse
import cl.rayout.adidastest.response.Result
import cl.rayout.adidastest.util.NonNullMediatorLiveData
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


@Suppress("EXPERIMENTAL_FEATURE_WARNING")
class MainActivityViewModel constructor(private val goalDataSource: GoalDataSource) : ViewModel() {
//    private var goalDao : GoalDao
//
//    private var aLLGoals : LiveData<List<Goal>>


    private val _goals = NonNullMediatorLiveData<List<GoalResponse.Goal>>()
    private val _errors = NonNullMediatorLiveData<String>()



    private var goalsJob : Job? = null



    val goals: LiveData<List<GoalResponse.Goal>> get() = _goals
    val errors: LiveData<String> get() = _errors

    init {
//        val database : GoalDatabase = GoalDatabase.getInstance(
//            application.applicationContext
//        )!!
//        goalDao = database.goalDao()
//        aLLGoals = goalDao.getAllGoals()
        initGetGoalsCall()
    }
    init {

    }
    private fun initGetGoalsCall() {

        goalsJob = launch {
            val value = goalDataSource.getGoals()


            when (value) {
                is Result.Success -> {
                    _goals.postValue(value.data.goals)

                }
                is Result.Error -> _errors.postValue(value.exception.message)
            }

        }
    }



    override fun onCleared() {
        super.onCleared()
        goalsJob?.cancel()
    }
}
