package cl.rayout.adidastest.activites.mainActivity.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import cl.rayout.adidastest.R
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        if (intent.hasExtra("id")) {
            val id = intent.getIntExtra("id", 1)
            val title = intent.getStringExtra("title")
            val description = intent.getStringExtra("description")
            val type = intent.getStringExtra("type")
            val goal = intent.getIntExtra("goal", 500)

            detail_id.text = id.toString()
            detail_description.text = description
            detail_title.text = title
            detail_type.text = type
            detail_goal.text = goal.toString()
        }
    }
}
