package cl.rayout.adidastest.backend

import cl.rayout.adidastest.response.GoalResponse
import kotlinx.coroutines.Deferred


import retrofit2.Response
import retrofit2.http.GET

interface ApiService {



    @GET(value = "_ah/api/myApi/v1/goals")
    fun getGoals(): Deferred<Response<GoalResponse>>
}