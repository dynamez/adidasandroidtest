package cl.rayout.adidastest.backend

import android.app.Application
import cl.rayout.adidastest.di.components.AppComponent
import cl.rayout.adidastest.di.components.DaggerAppComponent
import cl.rayout.adidastest.di.modules.ApplicationContextModule
import timber.log.Timber

class MyCustomApplicationClass : Application() {

    private lateinit var applicationComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        applicationComponent = DaggerAppComponent
            .builder()
            .applicationContextModule(ApplicationContextModule(this))
            .build()
    }

    fun getAppComponent() = applicationComponent
}